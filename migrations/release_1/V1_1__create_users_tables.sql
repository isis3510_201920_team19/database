CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    firebase_id CHAR(28) UNIQUE
);

CREATE TABLE thesis_users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(40) NOT NULL UNIQUE,
    inviter_email VARCHAR(40),
    extra_points INTEGER NOT NULL DEFAULT 0
);