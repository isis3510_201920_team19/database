CREATE TABLE user_bus_stop_distances (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT,
    bus_stop_id BIGINT,
    ts TIMESTAMP NOT NULL,
    distance SMALLINT
);