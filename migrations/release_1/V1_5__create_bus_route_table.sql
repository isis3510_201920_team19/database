CREATE TABLE bus_routes (
    id SERIAL PRIMARY KEY,
    route_name VARCHAR(40) UNIQUE NOT NULL
);