CREATE TABLE bus_stops (
    id SERIAL PRIMARY KEY,
    bus_stop_name VARCHAR(30) UNIQUE NOT NULL,
    latitude NUMERIC(8, 5) NOT NULL,
    longitude NUMERIC(9, 5) NOT NULL
);

CREATE TABLE wait_times (
    id SERIAL PRIMARY KEY,
    user_id SMALLINT NOT NULL,
    bus_stop_id INT NOT NULL REFERENCES bus_stops(id),
    start_ts TIMESTAMP NOT NULL,
    wait_time_s SMALLINT NOT NULL
);

CREATE INDEX ON wait_times (bus_stop_id);

CREATE TABLE thesis_wait_times (
    id SERIAL PRIMARY KEY,
    user_id SMALLINT NOT NULL,
    bus_stop VARCHAR(30) NOT NULL,
    start_ts TIMESTAMP NOT NULL,
    wait_time_s SMALLINT NOT NULL
);