CREATE TABLE locations (
    id BIGSERIAL PRIMARY KEY,
    user_id SMALLINT NOT NULL,
    ts TIMESTAMP NOT NULL,
    latitude NUMERIC(8, 5) NOT NULL,
    longitude NUMERIC(9, 5) NOT NULL,
    horizontal_accuracy SMALLINT NOT NULL,
    course SMALLINT NOT NULL,
    speed NUMERIC(5, 2) NOT NULL
);

CREATE INDEX ON locations (ts);

CREATE TABLE thesis_locations (
    id BIGSERIAL PRIMARY KEY,
    thesis_user_id SMALLINT NOT NULL,
    ts TIMESTAMP NOT NULL,
    latitude NUMERIC(8, 5) NOT NULL,
    longitude NUMERIC(9, 5) NOT NULL,
    horizontal_accuracy SMALLINT NOT NULL,
    course SMALLINT NOT NULL,
    speed NUMERIC(5, 2) NOT NULL
);

CREATE INDEX ON thesis_locations (ts);