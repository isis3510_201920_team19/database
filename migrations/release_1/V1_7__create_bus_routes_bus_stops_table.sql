ALTER TABLE bus_routes
ALTER COLUMN id SET DATA TYPE BIGINT;

ALTER TABLE bus_stops
RENAME COLUMN bus_stop_name TO name;

ALTER TABLE bus_stops
ALTER COLUMN id SET DATA TYPE BIGINT;

CREATE TABLE bus_routes_bus_stops (
    bus_route_id BIGINT REFERENCES bus_routes (id) ON UPDATE CASCADE ON DELETE CASCADE,
    bus_stop_id BIGINT REFERENCES bus_stops (id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT bus_routes_bus_stops_id PRIMARY KEY (bus_route_id, bus_stop_id)
);

CREATE INDEX ON bus_routes_bus_stops (bus_stop_id);