ALTER TABLE user_bus_stop_distances
RENAME COLUMN distance TO distance_m;

ALTER TABLE user_bus_stop_distances
ADD COLUMN walk_time_s SMALLINT;